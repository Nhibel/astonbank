/**
 *
 */
package com.aston.cam.bank;

import java.util.HashMap;
import java.util.Map;

import com.aston.cam.models.Account;
import com.aston.cam.models.AccountRemuneration;
import com.aston.cam.models.BankException;
import com.aston.cam.models.Customer;
import com.aston.cam.models.LimitedRemunerateAccount;

/**
 * @author camille
 *
 */
public class run {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create Map array
		Map <Integer, Account> accountsMap = new HashMap<Integer, Account>();

		// Create a customer
		Customer customer1 = new Customer(1, "Devos", "Camille", 34);
		Customer customerMap = new Customer(1, "Gordani", "Fred", 40, accountsMap);

		Account accountsMap1 = new Account(1, 1000);
		Account accountsMap2 = new Account(2, 2000);
		Account accountsMap3 = new Account(3, 3000);
		Account accountsMap4 = new Account(4, 4000);
		Account accountsMap5 = new Account(5, 5000);
		Account accountsMap6 = new Account(6, 6000);
		//accountsMap.put(10, accountMap1);

		try {
			customerMap.addAccountMap(1, accountsMap1);
			customerMap.addAccountMap(2, accountsMap2);
			customerMap.addAccountMap(3, accountsMap3);
			customerMap.addAccountMap(4, accountsMap4);
			customerMap.addAccountMap(5, accountsMap5);
		} catch (BankException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		System.out.println("Taille du Map : "+customerMap.getAccountMap().size());
		System.out.println(accountsMap.get(5));
		System.out.println("");



		// Create accounts
		Account account1 = new Account(1, 1000);
		Account account2 = new Account(2, 2000);

		// Add accounts for this customer
		try {
			customer1.addAccount(account1);
			customer1.addAccount(account2);
		} catch (BankException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("Avant modif de compte : " + account1);
		System.out.println("Avant modif de compte : " + account2);
		System.out.println("");

		// Test functions for adding or removing amounts
		account1.addAmount(50);
		try {
			account2.remove(100);
		} catch (BankException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("Après modif de compte : " + account1);
		System.out.println("Après modif de compte : " + account2);
		System.out.println("");

		System.out.println(customer1);
		System.out.println("");

		// Test function for searching an account
		try {
			System.out.println("test du getAccount : " + customer1.getAccount(1));
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			System.out.println("Compte non existant");
			System.out.println("");

		}

		// Test function for calculate interest of an account
		AccountRemuneration accountRemuneration1 = new AccountRemuneration(3, 1000, 0.1);
		AccountRemuneration accountRemuneration2 = new AccountRemuneration(4, 2000, 0.2);

		// Add accountRemuneration to customer
		try {
			customer1.addAccount(accountRemuneration1);
			customer1.addAccount(accountRemuneration2);
		} catch (BankException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		accountRemuneration1.calculateInterest();
		System.out.println("Calcul de l'intérêt : " + accountRemuneration1.calculateInterest());
		System.out.println("");

		// Look for account where we can pay interests, if account find, pay interest
		for (int i = 1; i <= customer1.getAccountsList().size(); i++) {
			if (customer1.getAccount(i) instanceof AccountRemuneration) {
				((AccountRemuneration) customer1.getAccount(i)).payInterest();
				System.out.println("Paiement intérêt : "+customer1.getAccount(i));
				System.out.println("");
			}
		}

		for (int i = 1; i <= customer1.getAccountsList().size(); i++) {
			System.out.println("état des comptes : " + customer1.getAccount(i));
		}

		System.out.println("");

		// Create a Limited Remunerated Account
		LimitedRemunerateAccount limitedRemunerateAccount1 = new LimitedRemunerateAccount(5, 5000, 0.9, 1000);
		System.out.println("test compte à seuil remunéré avant payInterest " + limitedRemunerateAccount1);
		System.out.println("");

		try {
			customer1.addAccount(limitedRemunerateAccount1);
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Pay interest on Limited Remunerated Account
		limitedRemunerateAccount1.payInterest();
		System.out.println("test compte à seuil remunéré après payInterest " + limitedRemunerateAccount1);
		System.out.println("");

		// Remove amount from account
		try {
			limitedRemunerateAccount1.remove(5000);
		} catch (BankException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("test compte à seuil remunéré remove amount " + limitedRemunerateAccount1);
	}
}
