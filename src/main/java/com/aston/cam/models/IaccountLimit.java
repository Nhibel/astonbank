package com.aston.cam.models;

public interface IaccountLimit {
	public void remove(double amount) throws BankException;
	public double getLimit();
	public void setLimit(double limit);
}
