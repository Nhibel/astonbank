package com.aston.cam.models;

import java.util.ArrayList;
import java.util.Map;

public class Customer {
	private String name, firstname;
	private int age, id;
	private ArrayList<Account> accountsList;
	private Map<Integer, Account> accountsMap;

	public Customer(int id, String name, String firstname, int age) {
		this.id = id;
		this.name = name;
		this.firstname = firstname;
		this.age = age;
		this.accountsList = new ArrayList<Account>();
	}

	public Customer(int id, String name, String firstname, int age, Map <Integer, Account > accountsMap ) {
		this.id = id;
		this.name = name;
		this.firstname = firstname;
		this.age = age;
		this.accountsMap = accountsMap;
	}

	/*
	 * @Author Cam
	 * @Param Account account
	 * Add account if list size inferior to given number
	 */
	public void addAccount(Account account) throws BankException {
		if (this.accountsList.size() > 4) {
			throw new BankException ("Vous ne pouvez pas ajouter plus de comptes");
		} else {
			this.accountsList.add(account);
		}
	}

	public void addAccountMap (int id, Account account) throws BankException {
		if (this.accountsMap.size() > 4 ) {
			throw new BankException ("Vous ne pouvez pas ajouter plus de comptes");
		} else {
			this.accountsMap.put(account.getAccountId(), account);
		}
	}

	/*
	 * @Author Cam
	 * @Param int nbr
	 * Get account by account id
	 */
	public Account getAccount(int id) {
		return this.accountsList.get(id-1);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Customer [name=");
		builder.append(this.name);
		builder.append(", firstname=");
		builder.append(this.firstname);
		builder.append(", age=");
		builder.append(this.age);
		builder.append(", id=");
		builder.append(this.id);
		builder.append(", accountsList=");
		builder.append(this.accountsList);
		builder.append(", accountsMap=");
		builder.append(this.accountsMap);
		builder.append("]");
		return builder.toString();
	}

	public Map<Integer, Account> getAccountsMap() {
		return this.accountsMap;
	}

	public void setAccountsMap(Map<Integer, Account> accountsMap) {
		this.accountsMap = accountsMap;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getNbr() {
		return this.id;
	}

	public void setNbr(int id) {
		this.id = id;
	}

	public ArrayList<Account> getAccountsList() {
		return this.accountsList;
	}

	public void setAccountList(ArrayList<Account> accountsList) {
		this.accountsList = accountsList;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Map<Integer, Account> getAccountMap() {
		return this.accountsMap;
	}

	public void setAccountMap(Map<Integer, Account> accountMap) {
		this.accountsMap = accountMap;
	}

	public void setAccountsList(ArrayList<Account> accountsList) {
		this.accountsList = accountsList;
	}

}
