package com.aston.cam.models;



public class Account {
	protected int accountId;
	protected double balance;

	public Account() {
	}

	public Account(int accountId, double balance) {
		this.accountId = accountId;
		this.balance = balance;
	}

	public void addAmount(double amount) {
		this.balance = this.balance + amount;
	}


	public void remove(double amount) throws BankException {
		if ((this.balance - amount) < 0) {
			throw new BankException ("Vous ne pouvez pas retirer plus d'argent");
		} else {
			this.balance = this.balance - amount;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [accountId=");
		builder.append(this.accountId);
		builder.append(", balance=");
		builder.append(this.balance);
		builder.append("]");
		return builder.toString();
	}

	// Getters & Setters
	public int getAccountId() {
		return this.accountId;
	}

	private void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public double getBalance() {
		return this.balance;
	}

	private void setBalance(double balance) {
		this.balance = balance;
	}

}
