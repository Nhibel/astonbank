package com.aston.cam.models;

public class LimitedRemunerateAccount extends AccountRemuneration implements IaccountLimit {

	private double limit;



	public LimitedRemunerateAccount(int nbr, double balance, double rate, double limit) {
		super(nbr, balance, rate);
		this.limit = limit;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void remove(double amount) throws BankException {
		if ((this.balance - amount) < this.limit) {
			throw new BankException ("Vous ne pouvez pas retirer plus d'argent");
		} else {
			this.balance = this.balance - amount;
		}
	}

	@Override
	public double getLimit() {
		return this.limit;
	}

	@Override
	public void setLimit(double limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LimitedRemunerateAccount [limit=");
		builder.append(this.limit);
		builder.append(", rate=");
		builder.append(this.getRate());
		builder.append(", accountId=");
		builder.append(this.accountId);
		builder.append(", balance=");
		builder.append(this.balance);
		builder.append("]");
		return builder.toString();
	}
}
