package com.aston.cam.models;

public class AccountLimit extends Account implements IaccountLimit {
	private double limit;

	public AccountLimit() {
	}

	public AccountLimit(int accountId, double balance, double limit) {
		super(accountId, balance);
		this.limit = limit;
	}

	@Override
	public void remove (double amount) throws BankException {
		if ((this.balance - amount) < this.limit) {
			throw new BankException ("Vous ne pouvez pas retirer plus d'argent");
		} else {
			this.balance = this.balance - amount;
		}
	}

	@Override
	public double getLimit() {
		return this.limit;
	}

	@Override
	public void setLimit(double limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountLimit [limit=");
		builder.append(this.limit);
		builder.append(", nbr=");
		builder.append(this.accountId);
		builder.append(", balance=");
		builder.append(this.balance);
		builder.append("]");
		return builder.toString();
	}

}
