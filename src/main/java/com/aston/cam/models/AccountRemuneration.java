package com.aston.cam.models;

public class AccountRemuneration extends Account implements IaccountRemuneration {
	private double rate;

	public AccountRemuneration() {}

	public AccountRemuneration(int nbr, double balance, double rate) {
		super(nbr, balance);
		this.setRate(rate);
	}

	/*
	 * @Author Cam
	 */
	@Override
	public double calculateInterest() {
		return (this.balance*this.rate);
	}

	@Override
	public void payInterest() {
		this.balance = this.balance + this.calculateInterest();
	}

	@Override
	public double getRate() {
		return this.rate;
	}

	@Override
	public void setRate(double rate) {
		if (rate < 0 || rate >= 1) {
			throw new IllegalArgumentException ("Le taux doit être compris entre 0 et 1");
		}
		this.rate = rate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountRemuneration [rate=");
		builder.append(this.rate);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
}
