package com.aston.cam.models;

public interface IaccountRemuneration {
	public double calculateInterest();
	public void payInterest();
	public double getRate();
	public void setRate(double rate);
}